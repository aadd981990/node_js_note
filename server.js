const express = require("express");
const bodyParser = require("body-parser");
const expressHbs = require("express-handlebars");
const nodeRouter = require("./router/nodesRouter");
const path = require("path");
const app = express();

app.use(
  "/src/css",
  express.static(path.join(__dirname, "node_modules/bootstrap/dist/css"))
);
app.use("/src/css", express.static(path.join(__dirname, "src/css")));
app.use("/src/js", express.static(path.join(__dirname, "src/js")));
app.use(
  "/src/js",
  express.static(path.join(__dirname, "node_modules/bootstrap/dist/js"))
);
app.use(
  "/src/js",
  express.static(path.join(__dirname, "node_modules/jquery/dist"))
);

app.engine("hbs", expressHbs({ defaultLayout: "main-layout", extname: "hbs" }));

app.set("view engine", "ejs");
app.set("views", "./views");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(nodeRouter);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Running on port ${PORT}`));
