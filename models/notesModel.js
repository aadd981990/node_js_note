const mongoConnect = require("../ultil/dataBase").mongoConnect;
const mongodb = require("mongodb");

// const getDb = require("../ultil/dataBase").getDb;

class Notes {
  constructor(title, body) {
    this.title = title;
    this.body = body;
  }

  save() {
    console.log(this);
    return new Promise((resolve, reject) => {
      mongoConnect((client) => {
        const db = client.db("node_js_notes");

        db.collection("notes")
          .insertOne(this)
          .then((result) => {
            console.log(result);
            resolve(result);
          })
          .catch((err) => {
            console.log(err);
            reject(err);
          });
      });
    });
  }

  update(noteId) {
    return new Promise((resolve, reject) => {
      mongoConnect((client) => {
        const db = client.db("node_js_notes");
        db.collection("notes")
          .updateOne({ _id: new mongodb.ObjectId(noteId) }, { $set: this })
          .then((result) => {
            console.log(result[0]);
            resolve(result);
          })
          .catch((err) => {
            console.log(err);
            reject(err);
          });
      });
    });
  }

  get() {
    let notes = [];
    return new Promise((resolve, reject) => {
      mongoConnect((client) => {
        const db = client.db("node_js_notes");
        db.collection("notes")
          .find()
          .toArray(function (err, results) {
            if (err) throw err;
            resolve(results);
          });
        client.close();
      });
    });
  }

  getById(noteId) {
    let notes = [];
    return new Promise((resolve, reject) => {
      mongoConnect((client) => {
        //   if (err) throw err;
        const db = client.db("node_js_notes");
        db.collection("notes")
          .find({ _id: new mongodb.ObjectId(noteId) })
          .toArray(function (err, results) {
            if (err) throw err;
            console.log(results);
            notes = results;
            resolve(notes);
            // return notes;
          });
        client.close();
      });
    });
  }

  search(title) {
    let notes = [];
    return new Promise((resolve, reject) => {
      mongoConnect((client) => {
        const db = client.db("node_js_notes");
        db.collection("notes")
          .find({ $text: { $search: title } })
          .toArray(function (err, results) {
            if (err) throw err;
            console.log(results);
            notes = results;
            resolve(notes);
          });
        client.close();
      });
    });
  }

  // delete(id) {
  //   return new Promise((resolve, reject) => {
  //     mongoConnect((client) => {
  //       const db = client.db("node_js_notes");
  //       db.collection("notes").deleteOne({ _id: new mongodb.ObjectId(id) });
  //       resolve(result);
  //     });
  //   });
  // }

  delete(id) {
    console.log(this);
    return new Promise((resolve, reject) => {
      mongoConnect((client) => {
        const db = client.db("node_js_notes");

        db.collection("notes")
          .deleteOne({ _id: new mongodb.ObjectId(id) })
          .then((result) => {
            console.log(result);
            resolve(result);
          })
          .catch((err) => {
            console.log(err);
            reject(err);
          });
      });
    });
  }
}

module.exports = Notes;
