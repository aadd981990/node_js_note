exports.createNote = (req, res) => {
  const title = req.body.title;
  const body = req.body.body;
  if (!title && !body) {
    res.redirect("back");
  }
  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData(title, body);
  notesModel
    .save()
    .then((result) => {
      res.redirect("/notes");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.editNote = (req, res) => {
  const id = req.params.noteId;

  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData("", "");
  notesModel.getById(id).then((result) => {
    res.render("editNote", {
      note: result[0],
      pageTitle: "Edit Note",
    });
  });
};

exports.search = (req, res, next) => {
  const searchText = req.body.search;
  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData();
  notesModel.get().then((results) => {
    const filtered = results.filter((note) => {
      if (
        note.body.toLowerCase().includes(searchText.toLowerCase()) ||
        note.title.toLowerCase().includes(searchText.toLowerCase())
      ) {
        return note;
      }
    });
    res.render("notes", {
      notes: filtered,
    });
  });
};

exports.updateNote = (req, res) => {
  const title = req.body.title;
  const body = req.body.body;
  const noteId = req.body._id;
  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData(title, body);
  notesModel
    .update(noteId)
    .then((result) => {
      res.redirect("/notes");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getNotes = (req, res, next) => {
  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData();
  notesModel.get().then((results) => {
    res.render("notes", {
      notes: results,
    });
  });
};

exports.getNoteById = (req, res, next) => {
  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData();
  const id = req.params.noteId;
  notesModel.getById(id).then((result) => {
    res.render("indexUpd", {
      prods: result,
    });
  });
};

exports.delete = (req, res, next) => {
  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData();
  const id = req.params.noteId;
  notesModel.delete(id).then((result) => {
    res.redirect("/notes");
  });
};

exports.mainForm = (req, res, next) => {
  res.render("form", { pageTitle: "add note", errorMessage: null });
};
exports.showNote = (req, res, next) => {
  const NotesData = require("../models/notesModel");
  const notesModel = new NotesData();
  const id = req.params.noteId;
  notesModel.getById(id).then((result) => {
    res.render("show", {
      prods: result,
    });
  });
};
