const express = require("express");
const router = express.Router();
const NotesController = require("../controllers/NotesController2");

router.post("/search", NotesController.search);
router.post("/add-note", NotesController.createNote);
router.post("/update-note", NotesController.updateNote);
router.get("/note/:noteId/show", NotesController.showNote);
router.get("/note/:noteId/edit", NotesController.editNote);
router.get("/note/:noteId/delete", NotesController.delete);
router.get("/form", NotesController.mainForm);
router.get("/", NotesController.mainForm);
router.get("/notes", NotesController.getNotes);
router.get("/formId/:noteId", NotesController.getNoteById);

module.exports = router;
